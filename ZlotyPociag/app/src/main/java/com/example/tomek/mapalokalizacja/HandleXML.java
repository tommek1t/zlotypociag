package com.example.tomek.mapalokalizacja;

/**
 * Created by Tomek on 23.03.2017.
 */
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import android.content.Context;
import android.location.Location;

public class HandleXML {
    private ArrayList<RSSNews> news=new ArrayList<RSSNews>();
    private String urlString = null;
    private XmlPullParserFactory xmlFactoryObject;
    public volatile boolean parsingComplete = true;
    ArrayList<Miasto> miasta=new ArrayList<Miasto>();

    public HandleXML(Location position){
        if(position==null)
            this.urlString="http://walbrzych.naszemiasto.pl/rss/artykuly/17.xml";

        addCities();
        double odleglosc=180;//po wspolrzednych
        int onList=0;
        for(int i=0;i<miasta.size();i++){
            double odlTemp=Math.sqrt((miasta.get(i).getLat()-position.getLatitude())*(miasta.get(i).getLat()-position.getLatitude()))
                    +((miasta.get(i).getLong()-position.getLongitude())*(miasta.get(i).getLong()-position.getLongitude()));
            if(odlTemp<=odleglosc){
                odleglosc=odlTemp;
                onList=i;
            }
        }
        this.urlString = miasta.get(onList).urlRSS;
    }

    private void addCities(){
        miasta.add(new Miasto(52.259,21.02,"Warszawa","http://warszawa.naszemiasto.pl/rss/artykuly/17.xml"));
        miasta.add(new Miasto(52.399,16.9,"Poznań","http://poznan.naszemiasto.pl/rss/artykuly/17.xml"));
        miasta.add(new Miasto(51.849,16.57,"Leszno","http://leszno.naszemiasto.pl/rss/artykuly/17.xml"));
        miasta.add(new Miasto(50.78,16.28,"Wałbrzych","http://walbrzych.naszemiasto.pl/rss/artykuly/17.xml"));
        miasta.add(new Miasto(51.11,17.03,"Wrocław","http://wroclaw.naszemiasto.pl/rss/artykuly/17.xml"));
        miasta.add(new Miasto(51.209,16.159,"Legnica","http://legnica.naszemiasto.pl/rss/artykuly/17.xml"));
        miasta.add(new Miasto(51.649,15.13,"Żary","http://zary.naszemiasto.pl/rss/artykuly/17.xml"));
    }

    public ArrayList<RSSNews> getNews(){
        return news;
    }

    public void parseXMLAndStoreIt(XmlPullParser myParser) {
        int event;
        String text=null;

        try {
            event = myParser.getEventType();
            RSSNews tempNews=new RSSNews();

            while (event != XmlPullParser.END_DOCUMENT) {
                String name=myParser.getName();

                switch (event){
                    case XmlPullParser.START_TAG:
                        break;

                    case XmlPullParser.TEXT:
                        text = myParser.getText();
                        break;

                    case XmlPullParser.END_TAG:
                        if(name.equals("title")){
                            tempNews.setTitle(text);
                        }
                        else if(name.equals("link")){
                            tempNews.setLink(text);
                        }
                        else if(name.equals("description")){
                           tempNews.setDescription(text);
                        }
                        else if (name.equals("item")){
                            news.add(new RSSNews(tempNews.getTitle(),tempNews.getLink(),tempNews.getDescription()));
                        }
                        break;
                }
                event = myParser.next();
            }
            parsingComplete = false;
        }

        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fetchXML(final Context c){
        final LoadingInfo l=new LoadingInfo(c,"Trwa pobieranie aktualności...");
        l.show();
        Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {
                try {
                    URL url = new URL(urlString);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();

                    conn.setReadTimeout(10000 /* milliseconds */);
                    conn.setConnectTimeout(15000 /* milliseconds */);
                    conn.setRequestMethod("GET");
                    conn.setDoInput(true);

                    // Starts the query
                    conn.connect();
                    InputStream stream = conn.getInputStream();

                    xmlFactoryObject = XmlPullParserFactory.newInstance();
                    XmlPullParser myparser = xmlFactoryObject.newPullParser();

                    myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                    myparser.setInput(stream, null);

                    parseXMLAndStoreIt(myparser);
                    stream.close();
                }

                catch (Exception e) {
                }
                l.dismiss();
            }
        });
        thread.start();
    }
}
