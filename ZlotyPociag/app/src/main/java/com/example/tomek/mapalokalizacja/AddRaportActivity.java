package com.example.tomek.mapalokalizacja;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

public class AddRaportActivity extends AppCompatActivity implements View.OnClickListener, View.OnLongClickListener {

    String name,phone,address,noteText, type;
    double lat,lng,rating=0.0;
    EditText note,nameEdit,phoneEdit,addressRaport;
    ImageView previewImage;

    String uriImage="default";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_raport);
        setTitle("Dodaj miejsce do raportu");

        Intent i = getIntent();
        Bundle przekazanedane = i.getExtras();
        name = przekazanedane.getString("name");
        phone=przekazanedane.getString("phone");
        address=przekazanedane.getString("address");
        noteText=przekazanedane.getString("note");
        type=przekazanedane.getString("type");
        rating=przekazanedane.getDouble("rating");
        lat=przekazanedane.getDouble("latitude");
        lng=przekazanedane.getDouble("longitude");

        nameEdit=(EditText) findViewById(R.id.nameNewRaport);
        nameEdit.setText(name);
        phoneEdit=(EditText) findViewById(R.id.phoneRaport);
        phoneEdit.setText(phone);
        TextView latlngLabel=(TextView) findViewById(R.id.latlngRaport);
        latlngLabel.setText(lat+","+lng);
        addressRaport=(EditText) findViewById(R.id.addressRaport);
        addressRaport.setText(address);
        TextView ratingLabel=(TextView) findViewById(R.id.ratingRaport);
        ratingLabel.setText("Ocena: "+rating);
        note=(EditText) findViewById(R.id.myNoteRaport);
        note.setText(noteText + " ");

        previewImage= (ImageView) findViewById((R.id.previewImage));
        if(i.hasExtra("imageUri")){
            uriImage=przekazanedane.getString("imageUri");
            useImage(Uri.parse(uriImage));
        }
        previewImage.setOnClickListener(this);
        previewImage.setOnLongClickListener(this);

        Button submit=(Button) findViewById(R.id.AddRaportButton);
        submit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.AddRaportButton:
                phone = phoneEdit.getText().toString();
                address = addressRaport.getText().toString();

                TinyDB tinydb = new TinyDB(this);
                ArrayList<String> names = tinydb.getListString("names");
                ArrayList lats = tinydb.getListDouble("lats");
                ArrayList lngs = tinydb.getListDouble("lngs");
                ArrayList<String> phones = tinydb.getListString("phones");
                ArrayList<String> addresses = tinydb.getListString("addresses");
                ArrayList<String> types = tinydb.getListString("types");
                ArrayList ratings = tinydb.getListDouble("ratings");
                ArrayList<String> notes = tinydb.getListString("notes");
                ArrayList<String> images = tinydb.getListString("images");

                if (lats.indexOf(lat) < 0 && lngs.indexOf(lng) < 0) {
                    name = nameEdit.getText().toString();
                    names.add(name);
                    phones.add(phone);
                    addresses.add(address);
                    types.add(type);
                    ratings.add(rating);
                    lats.add(lat);
                    lngs.add(lng);
                    notes.add(note.getText().toString());
                    images.add(uriImage);
                } else {
                    int position = names.indexOf(name);
                    name = nameEdit.getText().toString();
                    names.set(position, name);
                    phones.set(position, phone);
                    addresses.set(position, address);
                    types.set(position, type);
                    ratings.set(position, rating);
                    lats.set(position, lat);
                    lngs.set(position, lng);
                    notes.set(position, note.getText().toString());
                    images.set(position,uriImage);
                }

                tinydb.putListString("names", names);
                tinydb.putListString("phones", phones);
                tinydb.putListString("addresses", addresses);
                tinydb.putListString("types", types);
                tinydb.putListDouble("ratings", ratings);
                tinydb.putListDouble("lats", lats);
                tinydb.putListDouble("lngs", lngs);
                tinydb.putListString("notes", notes);
                tinydb.putListString("images", images);

                Intent in = new Intent(this, MyRaportActivity.class);
                startActivity(in);

                break;
            case R.id.previewImage:
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent,1);
                break;
        }
    }


    @Override
    public boolean onLongClick(View v) {
        previewImage.setImageResource(R.drawable.addimagedefault);
        uriImage="default";
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 1) {//===select_photo

            if (resultCode == RESULT_OK) {
                if (intent != null) {
                    // Get the URI of the selected file
                    final Uri uri = intent.getData();
                    uriImage=uri.toString();
                    useImage(uri);
                }
            }
            super.onActivityResult(requestCode, resultCode, intent);
        }
    }

    void useImage(Uri uri)
    {
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
            previewImage.setImageBitmap(bitmap);

        }catch (Exception e){System.out.println(e);}
    }
}
