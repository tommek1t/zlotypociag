package com.example.tomek.mapalokalizacja;

/**
 * Created by Tomek on 2017-03-13.
 */
public class Photo{
    int height;
    int width;
    String photo_reference="";

    public Photo(int height, int width, String photo_reference){
        this.height=height;
        this.width=width;
        this.photo_reference=photo_reference;
    }
}
