package com.example.tomek.mapalokalizacja;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Tomek on 08.04.2017.
 */

public class Miasto {
    LatLng position;
    String nazwa="";
    String urlRSS="";

public Miasto(Double lat,Double lng,String nazwa,String urlRSS){
    this.position=new LatLng(lat,lng);
    this.nazwa=nazwa;
    this.urlRSS=urlRSS;
}

    public double getLat(){
        return this.position.latitude;
    }

    public double getLong(){
        return this.position.longitude;
    }
}
