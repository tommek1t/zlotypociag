package com.example.tomek.mapalokalizacja;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.location.places.Places;

import java.util.concurrent.ExecutionException;

public class tagInfoActivity extends AppCompatActivity implements View.OnClickListener {
    String name,textInfo,tabName,placeId;
    TextView placeName, describe, address, phone_number,rating;
    ImageView photo;
    Miejsce place;

    GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tag_info);

        photo= (ImageView) findViewById((R.id.photo));
        photo.setImageResource(R.drawable.traina);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Drive.API)
                .addApi(Places.PLACE_DETECTION_API)
                .addApi(Places.GEO_DATA_API)
                .addScope(Drive.SCOPE_FILE)
                //.addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect(2);

        Intent i = getIntent();
        Bundle przekazanedane = i.getExtras();
        name = przekazanedane.getString("place_name");
        placeId=przekazanedane.getString("placeId");
        try {
            findPlaceById(placeId);
        }catch(Exception c){
            //reakcja na błąd znalezienia miejsca po ID
            int q=1;
        }


        tabName="miejsca";
        textInfo="\nTrwa ładowanie opisu miejsca... \n \n Jeszcze chwilka...";

        placeName = (TextView) findViewById (R.id.placeName);
        placeName.setText(name);

        describe = (TextView) findViewById (R.id.describe);
        describe.setText(textInfo);

        if(place!=null) {
            setTitle("Szczegóły "+place.name);
            address = (TextView) findViewById(R.id.address);
            address.setText("Adres: "+place.formatted_address);
            phone_number = (TextView) findViewById(R.id.phone_number);
            phone_number.setText("Telefon: "+place.formatted_phone_number);
            rating = (TextView) findViewById(R.id.rating);
            rating.setText("Srednia ocena miejsca: "+Double.toString(place.rating));
        }

        ImageView plus=(ImageView) findViewById(R.id.addImage);
        plus.setOnClickListener( this);
    }

    private String getUrl(String placeId) {
        StringBuilder googlePlacesUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/details/json?");
        googlePlacesUrl.append("placeid=" + placeId);
        googlePlacesUrl.append("&key=" + "AIzaSyDYwi3_GhlqNZybOGQtc9_LtyxZMCbD7BM");
        return (googlePlacesUrl.toString());
    }

    private void findPlaceById(String placeId) throws ExecutionException, InterruptedException {
        String url = getUrl(placeId);
        Object[] DataTransfer = new Object[2];
        DataTransfer[1]=this;
        DataTransfer[0] = url;
        GetDetailsData getDetailsData = new GetDetailsData(this);
        getDetailsData.execute(DataTransfer);
    }

    @Override
    public void onClick(View v) {
        Intent i=new Intent(this,AddRaportActivity.class);
        i.putExtra("name",this.name);
        i.putExtra("phone",this.place.formatted_phone_number);
        i.putExtra("address",this.place.formatted_address);
        i.putExtra("rating",this.place.rating);
        i.putExtra("type",this.place.type);
        i.putExtra("note"," ");
        i.putExtra("latitude",this.place.latLng.latitude);
        i.putExtra("longitude",this.place.latLng.longitude);
        startActivity(i);
    }
}

