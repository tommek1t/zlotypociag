package com.example.tomek.mapalokalizacja;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

/**
 * Created by Tomek on 09.03.2017.
 */

public class AboutDeveloperActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_developer);
        setTitle("O twórcy aplikacji...");
    }


    public static class Miejsce {
        public static int NUMBER_OF_PHOTOS=5;
        public static int NUMBER_OF_REVIEWS=5;

        public String formatted_address="";
        public String vicinity="";                //adres bez nazwy kraju i kodu (pocztowego)
        public String formatted_phone_number="";
        public String id="";
        public String place_id="";
        public String name="";
        public String international_phone_number="";
        public String[] open_hours;                //7 dni
        public Photo[] photos;
        public double rating;
        public Review[] reviews;

        public Miejsce(){
            this.photos=new Photo[NUMBER_OF_PHOTOS];
            this.reviews=new Review[NUMBER_OF_REVIEWS];
            this.open_hours=new String[7];
        }
    }
}
