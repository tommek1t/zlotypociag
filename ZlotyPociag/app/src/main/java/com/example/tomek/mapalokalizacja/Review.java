package com.example.tomek.mapalokalizacja;

/**
 * Created by Comarch on 2017-03-13.
 */
public class Review{
    String author_name="";
    int rating;
    String relative_time_description=""; //kiedy wystawiono opinię
    String text="";

    public Review(String author_name,int rating,String relative_time_description,String text){
        this.author_name=author_name;
        this.rating=rating;
        this.relative_time_description=relative_time_description;
        this.text=text;
    }
}
