package com.example.tomek.mapalokalizacja;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceLikelihood;
import com.google.android.gms.location.places.PlaceLikelihoodBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.android.gms.maps.model.TileProvider;
import com.google.android.gms.maps.model.UrlTileProvider;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback //aby była mapa
        , GoogleMap.OnInfoWindowClickListener,
        LocationListener { //do google places->googleAPIClient

    private GoogleMap mMap;
    private int PROXIMITY_RADIUS = 10000;
    Location mLastLocation, myLocation;
    Marker mCurrLocationMarker;
    public static ArrayList<HashMap<String, String>> placesList;
    MarkerOptions food, place, sleep, historical;
    String mapType;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        placesList = new ArrayList<HashMap<String, String>>();

        Intent i = getIntent();
        Bundle dane = i.getExtras();
        mapType = dane.getString("mapType");
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMinZoomPreference(13);
        myLocation=this.getCurrentLocation();

        //KOD OK, ALE CHWILOWO ZAKOMENTOWANY: LOGOWANIE NA KONTO
        //DLA localization
        if (ContextCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        } else {
            // Show rationale and request permission.
        }

        if(myLocation==null || !checkIfInArea(myLocation))
        {
            Toast.makeText(this,"Znajdujesz się poza obszarem, którego dotyczy aplikacja albo masz wyłączoną nawigację!",Toast.LENGTH_LONG).show();
            myLocation=new Location("");
            myLocation.setLatitude(50.8422547);//zamek Ksiaz
            myLocation.setLongitude(16.2896684);//zamek Ksiaz
        }

        historical = new MarkerOptions().icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)).snippet("Historyczne miejsce");
        food = new MarkerOptions().icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE)).snippet("Miejsce na posiłek");
        place = new MarkerOptions().icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).snippet("Warto zobaczyć");
        sleep = new MarkerOptions().icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)).snippet("Nocleg");

        tryGetData();
        setTiles(mapType);
        mMap.setOnInfoWindowClickListener(this);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(myLocation.getLatitude(),myLocation.getLongitude())));
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        Context context = getApplicationContext();
        Intent infoTag = new Intent(context, tagInfoActivity.class);
        LatLng posMarker = marker.getPosition();
        boolean exist = false;
        String placeId = "";
        String place_name = "";
        for (int i = 0; i < placesList.size() && (exist == false); i++) {
            String lat = placesList.get(i).get("lat");
            String lng = placesList.get(i).get("lng");
            if ((Double.toString(posMarker.latitude).contains(lat) && Double.toString(posMarker.longitude).contains(lng))) {
                placeId = placesList.get(i).get("placeId");
                place_name = placesList.get(i).get("place_name");
                exist = true;
            }
        }
        infoTag.putExtra("placeId", placeId);
        infoTag.putExtra("place_name", place_name);
        startActivity(infoTag);
    }

    public void tryGetData() {
        getTagsData("restaurant", food);
        getTagsData("lodging", sleep);
        getTagsData("museum", historical);
        getTagsData("park", place);//widoki/miejsca do zobaczenia
    }

    private void getTagsData(String typePlace, MarkerOptions marker) {
        String url = getUrl(myLocation.getLatitude(), myLocation.getLongitude(), typePlace);
        Object[] DataTransfer = new Object[2];
        DataTransfer[0] = mMap;
        DataTransfer[1] = url;
        Log.d("onClick", url);
        GetNearbyPlacesData getNearbyPlacesData = new GetNearbyPlacesData(marker,this);
        getNearbyPlacesData.execute(DataTransfer);
    }

    private String getUrl(double latitude, double longitude, String nearbyPlace) {
        StringBuilder googlePlacesUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlacesUrl.append("location=" + latitude + "," + longitude);
        googlePlacesUrl.append("&radius=" + PROXIMITY_RADIUS);
        googlePlacesUrl.append("&type=" + nearbyPlace);
        googlePlacesUrl.append("&sensor=true");
        googlePlacesUrl.append("&key=" + "AIzaSyDYwi3_GhlqNZybOGQtc9_LtyxZMCbD7BM");
        Log.d("getUrl", googlePlacesUrl.toString());
        return (googlePlacesUrl.toString());
    }

    public Location getCurrentLocation() {
        LocationManager locationManager = (LocationManager)
                getSystemService(Context.LOCATION_SERVICE);

        LocationListener locationListener = this;
        if(checkPermission()) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10, (android.location.LocationListener) locationListener);
          locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }
        return  locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
    }

    public boolean checkIfInArea(Location place){
        if(place.getLongitude()>15.0 && place.getLongitude()<17.4)//15.0 i 17.4
            if(place.getLatitude()>50.30 && place.getLatitude()<51.67)//50.30 i 51.67
                return true;
            else return false;
        else return false;
    }

    private boolean checkPermission(){
        return (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED);
    }

    @Override
    public void onLocationChanged(Location location) {

        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        mCurrLocationMarker = mMap.addMarker(markerOptions);

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11));
        Toast.makeText(MapsActivity.this, "Your Current Location", Toast.LENGTH_LONG).show();
        Log.d("onLocationChanged", "Exit");
    }

    public void setTiles(final String typeMaps) {
        TileProvider tileProvider = new UrlTileProvider(256, 256) {
            @Override
            public URL getTileUrl(int x, int y, int zoom) {
                String thunderforestService = "https://a.tile.thunderforest.com/"+typeMaps+"/%d/%d/%d.png?apikey=339c417c12324a3db171a8c8ba11b626";
            /* Define the URL pattern for the tile images */
                String s = String.format(thunderforestService,
                        zoom, x, y);
                if (!checkTileExists(x, y, zoom)) {
                    return null;
                }

                try {
                    return new URL(s);
                } catch (MalformedURLException e) {
                    throw new AssertionError(e);
                }
            }

            private boolean checkTileExists(int x, int y, int zoom) {
                int minZoom = 2;
                int maxZoom = 20;

                if ((zoom < minZoom || zoom > maxZoom)) {
                    return false;
                }

                return true;
            }
        };

        mMap.addTileOverlay(new TileOverlayOptions()
                .tileProvider(tileProvider));

    }

}