package com.example.tomek.mapalokalizacja;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MyRaportActivity extends AppCompatActivity implements View.OnClickListener,NavigationView.OnNavigationItemSelectedListener {
    ListView list;
    ArrayList<String> names,phones, addresses,notes,types,images;
    ArrayList<Double> ratings,lats,lngs;
    TinyDB tinydb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_raport);
        setTitle("Zbiór moich miejsc");

        tinydb = new TinyDB(this);
        names=tinydb.getListString("names");
        phones=tinydb.getListString("phones");
        addresses=tinydb.getListString("addresses");
        types=tinydb.getListString("types");
        ratings=tinydb.getListDouble("ratings");
        lats=tinydb.getListDouble("lats");
        lngs=tinydb.getListDouble("lngs");
        notes=tinydb.getListString("notes");
        images=tinydb.getListString("images");

        Button b=(Button)findViewById(R.id.addMyNewRapButton);
        b.setOnClickListener(this);
        if(names.size()>0)
        {
            b.setVisibility(View.GONE);
        }
        list = (ListView) findViewById(R.id.listMyRaport);
        ArrayAdapter la = new ArrayAdapter(this, android.R.layout.simple_list_item_1, names);
        list.setAdapter(la);
        final Context context=this;
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent in=new Intent(context,AddRaportActivity.class);
                in.putExtra("name", names.get(position));
                in.putExtra("phone",phones.get(position));
                in.putExtra("address",addresses.get(position));
                in.putExtra("type",types.get(position));
                in.putExtra("rating",(double)ratings.get(position));
                in.putExtra("latitude",(double)lats.get(position));
                in.putExtra("longitude",(double) lngs.get(position));
                in.putExtra("note",notes.get(position));
                in.putExtra("imageUri",images.get(position));
                startActivity(in);
            }
        });
        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener(){
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(),"Usuwanie pozycji... " , Toast.LENGTH_SHORT).show();

                deleteRecord(position);
                finish();
                startActivity(getIntent());
                return true;
            }
        });

    }

    public void deleteRecord(int position){
        names.remove(position);
        tinydb.putListString("names",names);
        phones.remove(position);
        tinydb.putListString("phones",phones);
        addresses.remove(position);
        tinydb.putListString("addresses",addresses);
        types.remove(position);
        tinydb.putListString("types",types);
        ratings.remove(position);
        tinydb.putListDouble("ratings",ratings);
        lats.remove(position);
        tinydb.putListDouble("lats",lats);
        lngs.remove(position);
        tinydb.putListDouble("lngs",lngs);
        notes.remove(position);
        tinydb.putListString("notes",notes);
        images.remove(position);
        tinydb.putListString("images",images);

        if(names.size()==0)
        {
            Button b=(Button)findViewById(R.id.addMyNewRapButton);
            b.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        //dla buttona do pierwszego dodania
        Intent in=new Intent(this,MapsActivity.class);
        in.putExtra("mapType","default");
        startActivity(in);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Context context = getApplicationContext();

        if (id == R.id.menu_main) {
            Intent i=new Intent(this,MenuMapsActivity.class);
            startActivity(i);
        }

        if(id==R.id.menu_main_raport){
            Intent in=new Intent(this,MyRaportActivity.class);
            startActivity(in);
        }

        if(id==R.id.menu_main_raport_maps){
            Intent in=new Intent(this,MyRaportMapsActivity.class);
            startActivity(in);
        }

        if (id == R.id.menu_main_neigbourhood) {
            Intent i=new Intent(this,MenuMapsActivity.class);
            startActivity(i);
        }

        if (id == R.id.nav_map_default) {
            Intent in = new Intent(this, MapsActivity.class);
            in.putExtra("mapType", "default");
            startActivity(in);
        } else if (id == R.id.nav_map_cycle) {
            Intent in = new Intent(this, MapsActivity.class);
            in.putExtra("mapType", "cycle");
            startActivity(in);
        } else if (id == R.id.nav_map_transport) {
            Intent in = new Intent(this, MapsActivity.class);
            in.putExtra("mapType", "transport");
            startActivity(in);
        } else if (id == R.id.nav_map_pioneer) {
            Intent in = new Intent(this, MapsActivity.class);
            in.putExtra("mapType", "pioneer");
            startActivity(in);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
