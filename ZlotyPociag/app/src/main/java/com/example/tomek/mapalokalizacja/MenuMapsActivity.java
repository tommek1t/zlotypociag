package com.example.tomek.mapalokalizacja;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.model.LatLng;

public class MenuMapsActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        com.google.android.gms.location.LocationListener {

    private HandleXML objXML;
    private Location myLocation;
    private ListView list;
    private double defaultLatitude = 50.8422547;//zamek Ksiaz
    private double defaultLongtitude = 16.2896684;//zamek Ksiaz

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_maps);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        myLocation = getCurrentLocation();
        if (myLocation == null) {
            myLocation = new Location("");
            myLocation.setLatitude(defaultLatitude);
            myLocation.setLongitude(defaultLongtitude);
        }
        objXML = new HandleXML(myLocation);
        objXML.fetchXML(this);
        while (objXML.parsingComplete) ;
        setListNews();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void setListNews() {
        list = (ListView) findViewById(R.id.listViewNews);
        final ListViewAdapter adapter = new ListViewAdapter(this, R.layout.row, objXML.getNews());
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MenuMapsActivity.this, OneNewsWebActivity.class);
                intent.putExtra("title", adapter.data.get(position).getTitle());
                intent.putExtra("url", adapter.data.get(position).getLink());
                MenuMapsActivity.this.startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_maps, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.about_app) {
            Intent in = new Intent(this, AboutAppActivity.class);
            startActivity(in);
        }
        if (id == R.id.about_developer) {
            Intent in = new Intent(this, AboutDeveloperActivity.class);
            startActivity(in);
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Context context = getApplicationContext();

        if (id == R.id.menu_main) {
            myLocation = new Location("");
            myLocation.setLatitude(defaultLatitude);
            myLocation.setLongitude(defaultLongtitude);
            this.prepareListNews();
        }

        if(id==R.id.menu_main_raport){
            Intent in=new Intent(this,MyRaportActivity.class);
            startActivity(in);
        }

        if(id==R.id.menu_main_raport_maps){
            Intent in=new Intent(this,MyRaportMapsActivity.class);
            startActivity(in);
        }

        if (id == R.id.menu_main_neigbourhood) {
            myLocation = getCurrentLocation();
            if (myLocation == null) {
                myLocation = new Location("");
                myLocation.setLatitude(defaultLatitude);
                myLocation.setLongitude(defaultLongtitude);
            }
            this.prepareListNews();
        }

        if (id == R.id.nav_map_default) {
            Intent in = new Intent(this, MapsActivity.class);
            in.putExtra("mapType", "default");
            startActivity(in);
        } else if (id == R.id.nav_map_cycle) {
            Intent in = new Intent(this, MapsActivity.class);
            in.putExtra("mapType", "cycle");
            startActivity(in);
        } else if (id == R.id.nav_map_transport) {
            Intent in = new Intent(this, MapsActivity.class);
            in.putExtra("mapType", "transport");
            startActivity(in);
        } else if (id == R.id.nav_map_pioneer) {
            Intent in = new Intent(this, MapsActivity.class);
            in.putExtra("mapType", "pioneer");
            startActivity(in);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void prepareListNews() {
        objXML = new HandleXML(myLocation);
        objXML.fetchXML(this);
        while (objXML.parsingComplete) ;
        setListNews();
    }

    public Location getCurrentLocation() {
        LocationManager locationManager = (LocationManager)
                getSystemService(Context.LOCATION_SERVICE);

        LocationListener locationListener = this;
        if (checkPermission()) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10, (android.location.LocationListener) this);
            locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }
        return locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
    }

    private boolean checkPermission() {
        return (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED);
    }

    @Override
    public void onLocationChanged(Location location) {
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
    }

    public static Context ctx;
    public static Context getLastSetContext() {
        return (Context) ctx;
    }

}

