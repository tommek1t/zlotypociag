package com.example.tomek.mapalokalizacja;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Tomek
 */

public class GetImageTask extends AsyncTask<String, Integer, Bitmap> {
    LoadingInfo l;
    Context context;
    boolean isLoader;

    public GetImageTask(Context c,boolean showLoader){
        context=c;
        isLoader=showLoader;
    }

    @Override
    protected Bitmap doInBackground(String... arg0) {
        Bitmap b = null;
        try {
            b = BitmapFactory.decodeStream((InputStream) new URL(arg0[0]).getContent());
            if(isLoader) l.dismiss();
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return b;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(isLoader) {
            l = new LoadingInfo(context, "Trwa pobieranie grafiki...");
            l.show();
        }
    }
}