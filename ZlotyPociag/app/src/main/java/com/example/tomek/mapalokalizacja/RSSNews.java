package com.example.tomek.mapalokalizacja;

/**
 * Created by Tomek on 23.03.2017.
 */

public class RSSNews {
    private String title = "title";
    private String link = "link";
    private String description = "description";
    public String srcImage="";
    public int widthImage=0;

    public RSSNews(){
    }

    public RSSNews(String title, String link, String description){
        setTitle(title);
        setDescription(prepareDescription(description));
        setLink(link);
    }

    public String getTitle(){
        return title;
    }

    public String getLink(){
        return link;
    }

    public String getDescription(){
        return description;
    }

    public void setTitle(String title){
        this.title=title;
    }

    public void setLink(String link){
        this.link=link;
    }

    public void setDescription(String description){
        this.description=description;
    }

    private String prepareDescription(String text){
        String descr=text.substring(text.indexOf("\"/>")+6,text.length()-1);
        System.out.print(text);
        String[] imgParam=text.split("\"");
        this.srcImage=imgParam[7];
        this.widthImage=Integer.parseInt(imgParam[5]);
        return descr;
    }
}
