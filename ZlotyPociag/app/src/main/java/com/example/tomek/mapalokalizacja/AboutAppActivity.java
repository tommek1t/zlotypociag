package com.example.tomek.mapalokalizacja;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

/**
 * Created by Tomek on 05.03.2017.
 */

public class AboutAppActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_app);
        setTitle("O aplikacji...");
    }
}
