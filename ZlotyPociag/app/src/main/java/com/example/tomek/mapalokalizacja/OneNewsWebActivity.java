package com.example.tomek.mapalokalizacja;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class OneNewsWebActivity extends AppCompatActivity {

    WebView webView;
    String url;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_news_web);
        webView=(WebView) findViewById(R.id.webView);

        Intent i = getIntent();
        Bundle przekazanedane = i.getExtras();
        url = przekazanedane.getString("url");
        setTitle(przekazanedane.getString("title"));
        webView.loadUrl(url);
    }
}
