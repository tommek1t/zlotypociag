package com.example.tomek.mapalokalizacja;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by Tomek on 22.04.2017.
 */

public class LoadingInfo {
    private ProgressDialog pd;
    public LoadingInfo(Context c,String message){
        pd=new ProgressDialog(c);
        pd.setMessage(message);
        pd.setCancelable(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    public void show(){
        pd.show();
    }

    public void hide(){
        pd.hide();
    }

    public void dismiss() {
        pd.dismiss();
    }
}
