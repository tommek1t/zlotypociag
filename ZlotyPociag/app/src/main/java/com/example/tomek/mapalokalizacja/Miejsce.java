package com.example.tomek.mapalokalizacja;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Tomek on 11.03.2017.
 */

public class Miejsce {
    public static int NUMBER_OF_PHOTOS=5;
    public static int NUMBER_OF_REVIEWS=5;

    public String formatted_address="brak dokładnego adresu";
    public String vicinity="";                  //adres bez nazwy kraju i kodu (pocztowego)
    public String formatted_phone_number="nie podano";
    public String id="";
    public String place_id="";
    public String name=" :)";
    public String international_phone_number="nie podano";
    public String[] open_hours;                //7 dni
    public Photo[] photos;
    public double rating;
    public Review[] reviews;
    public String type;
    public LatLng latLng;

    public Miejsce(){
        this.photos=new Photo[NUMBER_OF_PHOTOS];
        this.reviews=new Review[NUMBER_OF_REVIEWS];
        this.open_hours=new String[7];
    }

    public void setLatLng(double latitude,double longtitude){
        latLng=new LatLng(latitude,longtitude);
    }
}

