package com.example.tomek.mapalokalizacja;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceLikelihood;
import com.google.android.gms.location.places.PlaceLikelihoodBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.android.gms.maps.model.TileProvider;
import com.google.android.gms.maps.model.UrlTileProvider;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class MyRaportMapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener,LocationListener {

    private GoogleMap mMap;
    private int PROXIMITY_RADIUS = 10000;
    Location mLastLocation, myLocation;
    Marker mCurrLocationMarker;
    ArrayList<String> names;
    ArrayList lats,lngs;
    String mapType="pioneer";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMinZoomPreference(13);
        myLocation=this.getCurrentLocation();

        if(myLocation==null || !checkIfInArea(myLocation))
        {
            Toast.makeText(this,"Znajdujesz się poza obszarem, którego dotyczy aplikacja albo masz wyłączoną nawigację!",Toast.LENGTH_LONG).show();
            myLocation=new Location("");
            myLocation.setLatitude(50.8422547);//zamek Ksiaz
            myLocation.setLongitude(16.2896684);//zamek Ksiaz
        }

        setTiles(mapType);
        mMap.setOnInfoWindowClickListener(this);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(myLocation.getLatitude(),myLocation.getLongitude())));

        loadData();
    }

    private void loadData() {
        TinyDB tinydb=new TinyDB(this);
        names = tinydb.getListString("names");
        lats = tinydb.getListDouble("lats");
        lngs = tinydb.getListDouble("lngs");
        if(names.size()>0)
            Toast.makeText(this,"Zebrano "+names.size()+" miejsc na mapie.",Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(this,"Nie dodano jeszcze żadnych miejsc.",Toast.LENGTH_SHORT).show();

        for (int i=0;i<names.size();i++){
            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng((double)lats.get(i), (double)lngs.get(i)))
                    .title(names.get(i)));
        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        Context context = getApplicationContext();
        Intent infoTag = new Intent(context, tagInfoActivity.class);
        LatLng posMarker = marker.getPosition();
        boolean exist = false;
        String place_name = "";
        int position=-1;
        for (int i = 0; i < names.size() && (position<0); i++) {
            double lat =(double) lats.get(i);
            double lng = (double) lngs.get(i);
            if (posMarker.latitude==lat && posMarker.longitude==lng) {
                position=i;
            }
        }

        TinyDB tinydb = new TinyDB(this);
        ArrayList<String> names = tinydb.getListString("names");
        ArrayList lats = tinydb.getListDouble("lats");
        ArrayList lngs = tinydb.getListDouble("lngs");
        ArrayList<String> phones = tinydb.getListString("phones");
        ArrayList<String> addresses = tinydb.getListString("addresses");
        ArrayList<String> types = tinydb.getListString("types");
        ArrayList ratings = tinydb.getListDouble("ratings");
        ArrayList<String> notes = tinydb.getListString("notes");
        ArrayList<String> images = tinydb.getListString("images");
        
        Intent in=new Intent(context,AddRaportActivity.class);
        in.putExtra("name", names.get(position));
        in.putExtra("phone",phones.get(position));
        in.putExtra("address",addresses.get(position));
        in.putExtra("type",types.get(position));
        in.putExtra("rating",(double)ratings.get(position));
        in.putExtra("latitude",(double)lats.get(position));
        in.putExtra("longitude",(double) lngs.get(position));
        in.putExtra("note",notes.get(position));
        in.putExtra("imageUri",images.get(position));
        startActivity(in);
    }


    public Location getCurrentLocation() {
        LocationManager locationManager = (LocationManager)
                getSystemService(Context.LOCATION_SERVICE);

        LocationListener locationListener = this;
        if(checkPermission()) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10, (android.location.LocationListener) locationListener);
            locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }
        return  locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
    }

    public boolean checkIfInArea(Location place){
        if(place.getLongitude()>15.0 && place.getLongitude()<17.4)//15.0 i 17.4
            if(place.getLatitude()>50.30 && place.getLatitude()<51.67)//50.30 i 51.67
                return true;
            else return false;
        else return false;
    }

    private boolean checkPermission(){
        return (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED);
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("onLocationChanged", "entered");

        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        mCurrLocationMarker = mMap.addMarker(markerOptions);

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11));

        Log.d("onLocationChanged", "Exit");
    }

    public void setTiles(final String typeMaps) {
        TileProvider tileProvider = new UrlTileProvider(256, 256) {
            @Override
            public URL getTileUrl(int x, int y, int zoom) {
                String thunderforestService = "https://a.tile.thunderforest.com/"+typeMaps+"/%d/%d/%d.png?apikey=339c417c12324a3db171a8c8ba11b626";
            /* Define the URL pattern for the tile images */
                String s = String.format(thunderforestService,
                        zoom, x, y);
                if (!checkTileExists(x, y, zoom)) {
                    return null;
                }

                try {
                    return new URL(s);
                } catch (MalformedURLException e) {
                    throw new AssertionError(e);
                }
            }

            private boolean checkTileExists(int x, int y, int zoom) {
                int minZoom = 2;
                int maxZoom = 20;

                if ((zoom < minZoom || zoom > maxZoom)) {
                    return false;
                }

                return true;
            }
        };

        mMap.addTileOverlay(new TileOverlayOptions()
                .tileProvider(tileProvider));

    }

}
