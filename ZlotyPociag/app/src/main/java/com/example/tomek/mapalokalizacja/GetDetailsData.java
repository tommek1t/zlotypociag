package com.example.tomek.mapalokalizacja;

/**
 * Created by Tomek on 09.03.2017.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

public class GetDetailsData extends AsyncTask<Object, String, String> {

    String googlePlacesData;

    String url;
    Miejsce place;
    tagInfoActivity t;
    Context context;
    LoadingInfo l;

    public GetDetailsData(Context c){
        context=c;
    }

    @Override
    protected String doInBackground(Object... params) {
        try {
            url = (String) params[0];
            t=(tagInfoActivity)params[1];
            DownloadUrl downloadUrl = new DownloadUrl();
            googlePlacesData = downloadUrl.readUrl(url);
            Log.d("GooglePlacesReadTask", "doInBackground Exit");
        } catch (Exception e) {
            Log.d("GooglePlacesReadTask", e.toString());
        }
        return googlePlacesData;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        l=new LoadingInfo(context,"Trwa pobieranie danych z Internetu...");
        l.show();
    }

    @Override
    protected void onPostExecute(String result) {
        DataDetailParser dataParser = new DataDetailParser();
        place =  dataParser.parse(result);
        giveDetails();
        if(place.photos.length>0 && place.photos[0]!=null)
            getPhoto(place.photos[0].photo_reference);
        l.dismiss();
    }

    private void giveDetails() {
        t.place=this.place;
        t.setTitle("Szczegóły "+place.name);
        TextView address = (TextView) t.findViewById(R.id.address);
        address.setText("Adres: "+place.formatted_address);
        TextView phone_number = (TextView) t.findViewById(R.id.phone_number);
        phone_number.setText("Telefon: "+place.formatted_phone_number);
        TextView rating = (TextView) t.findViewById(R.id.rating);
        rating.setText("Srednia ocena miejsca: "+Double.toString(place.rating));
        ImageView icoType=(ImageView)t.findViewById(R.id.iconType);
        if(place.type.contains("lodging"))
            icoType.setImageResource(R.drawable.ico_sleep);
        if(place.type.contains("restaurant"))
            icoType.setImageResource(R.drawable.ico_food);
        if(place.type.contains("museum"))
            icoType.setImageResource(R.drawable.ico_historical);
        if(place.type.contains("park"))
            icoType.setImageResource(R.drawable.ico_place);
        if(place.reviews.length>0 && place.reviews[0]!=null)
            t.describe.setText(place.reviews[0].text);
        else
            t.describe.setText("Opis miejsca już wkrótce!");
    }

    private void getPhoto(String photoReference) {
        new GetImageTask(context,true) {
            @Override
            protected void onPostExecute(Bitmap result) {
                if (result != null) {
                    t.photo.setImageBitmap(result);
                }
            }
        }.execute(this.getPhotoUrl(photoReference));
    }

    private String getPhotoUrl(String photoReference){
        StringBuilder googlePlacesPhotoUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/photo?");
        googlePlacesPhotoUrl.append("maxwidth=" +"1400");
        //googlePlacesPhotoUrl.append("maxheight=" + "400");
        googlePlacesPhotoUrl.append("&photoreference=" + photoReference);
        googlePlacesPhotoUrl.append("&key=" + "AIzaSyDYwi3_GhlqNZybOGQtc9_LtyxZMCbD7BM");
        return (googlePlacesPhotoUrl.toString());

    }
}

