package com.example.tomek.mapalokalizacja;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Tomek on 09.03.2017.
 */


public class GetNearbyPlacesData extends AsyncTask<Object, String, String> {
        String googlePlacesData;
        GoogleMap mMap;
        String url;
        MarkerOptions typeMarker;
        LoadingInfo l;
        Context context;
        List<HashMap<String, String>> nearbyPlacesList = null;

        public GetNearbyPlacesData(MarkerOptions typeMarker, Context context){
                this.typeMarker=typeMarker;
                this.context=context;
        }

@Override
protected String doInBackground(Object... params) {
        try {
        Log.d("GetNearbyPlacesData", "doInBackground entered");
        mMap = (GoogleMap) params[0];
        url = (String) params[1];
        DownloadUrl downloadUrl = new DownloadUrl();
        googlePlacesData = downloadUrl.readUrl(url);
        Log.d("GooglePlacesReadTask", "doInBackground Exit");
        } catch (Exception e) {
        Log.d("GooglePlacesReadTask", e.toString());
        }
        return googlePlacesData;
        }

        @Override
        protected void onPreExecute() {
                super.onPreExecute();
                l = new LoadingInfo(context, "Trwa aktualizacja miejsc na mapie...");
                l.show();
        }

        @Override
protected void onPostExecute(String result) {
        Log.d("GooglePlacesReadTask", "onPostExecute Entered");
        DataParser dataParser = new DataParser();
        nearbyPlacesList =  dataParser.parse(result);
        ShowNearbyPlaces(nearbyPlacesList);
        Log.d("GooglePlacesReadTask", "onPostExecute Exit");
        MapsActivity.placesList.addAll(nearbyPlacesList);
                l.dismiss();
        }

private void ShowNearbyPlaces(List<HashMap<String, String>> nearbyPlacesList) {
        for (int i = 0; i < nearbyPlacesList.size(); i++) {
        Log.d("onPostExecute","Entered into showing locations");
        HashMap<String, String> googlePlace = nearbyPlacesList.get(i);
        double lat = Double.parseDouble(googlePlace.get("lat"));
        double lng = Double.parseDouble(googlePlace.get("lng"));
        String placeName = googlePlace.get("place_name");
        String vicinity = googlePlace.get("vicinity");
        LatLng latLng = new LatLng(lat, lng);
        typeMarker.position(latLng);
                typeMarker.title(placeName + " : " + vicinity);
        mMap.addMarker(typeMarker);
        //move map camera
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11));
        }
    }
}
