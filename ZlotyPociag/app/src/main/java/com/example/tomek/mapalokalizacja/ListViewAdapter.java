package com.example.tomek.mapalokalizacja;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Tomek on 23.03.2017.
 */
public class ListViewAdapter extends ArrayAdapter<RSSNews>{
    Context context;
    int layoutResourceId;
    ArrayList<RSSNews> data = null;

    public ListViewAdapter(Context context, int layoutResourceId,ArrayList<RSSNews> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        RSSNewsHolder holder = null;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new RSSNewsHolder();
            holder.describe = (TextView) row.findViewById(R.id.description_row);
            holder.title = (TextView)row.findViewById(R.id.title_row);
            holder.imageNews=(ImageView) row.findViewById(R.id.newsImage);
            row.setTag(holder);
        }
        else
        {
            holder = (RSSNewsHolder)row.getTag();
        }
        RSSNews news = data.get(position);
        holder.title.setText(news.getTitle());
        holder.describe.setText(news.getDescription());
        getPhoto(news.srcImage,holder.imageNews);
        return row;
    }

    private void getPhoto(String photoReference, final ImageView img) {
        new GetImageTask(context,false) {
            @Override
            protected void onPostExecute(Bitmap result) {
                if (result != null) {
                    img.setImageBitmap(result);
                }
            }
        }.execute(photoReference);
    }

    static class RSSNewsHolder
    {
        ImageView imageNews;
        TextView title;
        TextView describe;
        String link;
    }
}
