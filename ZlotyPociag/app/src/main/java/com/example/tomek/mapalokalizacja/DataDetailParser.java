package com.example.tomek.mapalokalizacja;

/**
 * Created by Tomek on 09.03.2017.
 */

import android.util.Log;

import com.google.android.gms.location.places.Place;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataDetailParser {
    JSONArray jsonArrayPhotos,jsonArrayReviews;
    JSONObject jsonObject,JSONMiejsce;
    Miejsce miejsce=new Miejsce();

    public Miejsce parse(String jsonData) {
        try {
            Log.d("Places", "parse");
            jsonObject = new JSONObject((String) jsonData);
            JSONMiejsce= (JSONObject) jsonObject.get("result");
            if(!JSONMiejsce.isNull("formatted_address"))miejsce.formatted_address=(String)JSONMiejsce.get("formatted_address");
            if(!JSONMiejsce.isNull("formatted_phone_number")) miejsce.formatted_phone_number=(String)JSONMiejsce.get("formatted_phone_number");
            if(!JSONMiejsce.isNull("vicinity")) miejsce.vicinity=(String)JSONMiejsce.get("vicinity");
            miejsce.id=(String)JSONMiejsce.get("id");
            miejsce.place_id=(String)JSONMiejsce.get("place_id");
            miejsce.name=(String)JSONMiejsce.get("name");
            if(!JSONMiejsce.isNull("international_phone_number")) miejsce.international_phone_number=(String)JSONMiejsce.get("international_phone_number");
            if(!JSONMiejsce.isNull("rating"))
                try{miejsce.rating=(double)JSONMiejsce.get("rating");}
                catch(Exception e){
                    miejsce.rating=(int)JSONMiejsce.get("rating");}

            double lat=JSONMiejsce.getJSONObject("geometry").getJSONObject("location").getDouble("lat");
            double lng=JSONMiejsce.getJSONObject("geometry").getJSONObject("location").getDouble("lng");
            miejsce.setLatLng(lat,lng);

            JSONObject hours;
            if(!JSONMiejsce.isNull("opening_hours")) {
                hours = (JSONObject) JSONMiejsce.get("opening_hours");
                JSONArray d=hours.getJSONArray("weekday_text");
                for(int i=0;i<d.length();i++){
                    miejsce.open_hours[i]=(String)d.get(i);
                }
            }

            JSONArray types;
            if(!JSONMiejsce.isNull("types")) {
                 types =JSONMiejsce.getJSONArray("types");
                for(int i=0;i<types.length();i++){
                    String type=(String)types.get(i);
                    if(type.contains("restaurant")||type.contains("lodging")||type.contains("museum")||type.contains("park"))
                        miejsce.type=(String)types.get(i);
                }
            }

            if(!JSONMiejsce.isNull("reviews")) {
                jsonArrayReviews = JSONMiejsce.getJSONArray("reviews");
                for (int i = 0; i < 5 && i < jsonArrayReviews.length(); i++) {
                    JSONObject r = (JSONObject) jsonArrayReviews.get(i);
                    miejsce.reviews[i] = new Review((String) r.get("author_name"), (int) r.get("rating"), (String) r.get("relative_time_description"), (String) r.get("text"));
                }
            }

            if(!JSONMiejsce.isNull("photos")) {
                jsonArrayPhotos = JSONMiejsce.getJSONArray("photos");
                for (int i = 0; i < 5 && i < jsonArrayPhotos.length(); i++) {
                    JSONObject p = (JSONObject) jsonArrayPhotos.get(i);
                    miejsce.photos[i] = new Photo((int) p.get("height"), (int) p.get("width"), (String) p.get("photo_reference"));
                }
            }


        } catch (JSONException e) {
            Log.d("Places", "parse error");
            e.printStackTrace();
        }
        return miejsce;
    }
}

