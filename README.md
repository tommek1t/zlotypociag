# Złoty Pociąg #

Aplikacja "Złoty Pociąg" jest aplikacją turystyczną dotyczącą regionu dolnegośląska dla amatorów - poszukiwaczy "skarbów" w tym regionie. Motywem przewodnim jest legenda o złotym pociągu, który rzekomo został ukryty pod Wałbrzychem i czeka tam do dziś na odkrywców.
Aplikacja powstała w ramach pracy inżynierskiej w roku 2017.

### Możliwości aplikacji ###

Możliwości aplikacji oraz narzędzia, z których się korzystało podczas jej tworzenia omawia prezentacja (plik Prezentacja.pdf). To, co wyróżnia aplikację od innych rozwiązań to możliwość wykorzystania Tile Overlays w celu lepszego dopasowania wyglądu i specyfiki mapy cufrowej do użytkownika.

### Informacje ogólne ###

Aplikacja była tworzona w Android Studio na system Android w wersji 6.0. 

### Who do I talk to? ###

Wszelkie uwagi oraz kontakt z autorem pod adresem: tommek1t[at]gmail.com